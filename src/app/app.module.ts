import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment.prod';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { UserFormComponent } from './user-form/user-form.component';
import { FormsModule } from '@angular/forms'
import { AuthGuard } from './auth.guard';

import { from } from 'rxjs';
import { SignupComponent } from './signup/signup.component';
import { UsersDetailsComponent } from './users-details/users-details.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserFormComponent,
    SignupComponent,
    UsersDetailsComponent,
    AdminPanelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
