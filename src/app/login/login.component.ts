import { Component, OnInit } from "@angular/core";
import { UserService } from "../user/user.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  user: string;
  pass: string;
  vald: boolean = false;
  showEmailerror:boolean=false;
  showPassworderror:boolean=false;
  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {}

  login(loginObj) {
    let checkin= false;
    this.showEmailerror=false;
    this.showPassworderror=false;
    if(this.user==""||this.user==undefined||this.user==null){
this.showEmailerror=true;
    }
    if(this.pass==""||this.pass==undefined||this.pass==null){
      this.showPassworderror=true;
    }
    if(!this.showEmailerror&&  !this.showPassworderror){
      if(this.user=="Admin"&&this.pass=="Admin@321"){
        this.router.navigateByUrl("Admin");
      }
      else{
        let UsersInf=[];
        this.userService.getAllUSerList().subscribe((data) => {
          let keys = [];
          keys = Object.keys(data);
          keys.forEach((key, index) => {
            UsersInf.push({'userName':data[key].userName,'succes': data[key].successCount,'error': data[key].errorCount,
            'Maxerror': data[key].Maxerror,
            'formassigned': data[key].formsAssigned,
            'formsubmitted': data[key].formsSubmitted,
            'password': data[key].passWord,
            'status': data[key].status}); 
          });
          UsersInf.forEach(element => {
            if(element.userName==loginObj.user)
            {
      
              if(element.password == this.pass &&
                element.status == "Active"&&
                element.error< element.Maxerror){
                  this.vald = true;
                  checkin=true;
                  this.userService.setUser(element.userName);
                  if (this.userService.getValidateUser()) {
                    this.router.navigate(["user"]);
                  }
                }
                else if (element.status !="Active") {
                  this.vald = false;
                  checkin=true;
                  alert(
                    "User You entered is expired."
                  );
                }
                else if( element.error>= element.Maxerror){
                  this.vald = false;
                  checkin=true;
                  alert(
                    "You Reach Maximum Error count."
                  );
                }
            }
        
          });
          if(checkin==false){
            this.vald = false;
           alert("user credentials are wrong!");
          }
        });  
       
      
    // this.userService.checkUsers(loginObj.user).subscribe((data) => {
    //   if (data[Object.keys(data)[0]] !== undefined) {
    //     if (
    //       data[Object.keys(data)[0]].userName == this.user &&
    //       data[Object.keys(data)[0]].passWord == this.pass &&
    //        data[Object.keys(data)[0]].status == "Active"&&
    //        data[Object.keys(data)[0]].errorCount< data[Object.keys(data)[0]].Maxerror
    //     ) {
    //       this.vald = true;
    //       this.userService.setUser(data[Object.keys(data)[0]].userName);
    //       if (this.userService.getValidateUser()) {
    //         this.router.navigate(["user"]);
    //       }
    //     }
    //     else if (data[Object.keys(data)[0]].status !="Active") {
    //       this.vald = false;
    //       alert(
    //         "User You entered is expired."
    //       );
    //     }
    //     else if( data[Object.keys(data)[0]].errorCount>= data[Object.keys(data)[0]].Maxerror){
    //       this.vald = false;
    //       alert(
    //         "You Reach Maximum Error count."
    //       );
    //     }
    //   } else {
    //     this.vald = false;
    //     alert("user credentials are wrong!");
    //   }
    // });
  }
  }
  }
}
