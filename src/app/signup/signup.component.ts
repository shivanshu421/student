import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { UserService } from '../user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  user: string;
  pass: string;
  formassigned: number;
  showEmailerror: boolean;
  showFormAssignederror: boolean;
  showPassworderror: boolean;

  constructor(private apiService: ApiService,private userService: UserService,private router: Router) { }

  ngOnInit(): void {
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  checkUserExistingorNot(usercheck:string){
    this.userService.checkUsers(this.user).subscribe((data) => {
      console.log(data);
      if(Object.keys(data)[0]===undefined){
        this.apiService.addUser(
          this.user,
          this.pass,
          this.formassigned,
          "Active",
          0,
          0,
          0,
          20
        ).subscribe((data) => {
          console.log(data)
          alert("user added Successfully");
        })
      }
      else{
        alert("user allready exist");
      }
     
    }
     
    )
    

  }
  usersDetails(){
    this.router.navigateByUrl("UsersDetails");
    
  }
  NavigatetLogin(){
    this.router.navigateByUrl("login"); 
  }
  signup(loginObj) {
    this.showEmailerror=false;
    this.showPassworderror=false;
    this.showFormAssignederror = false;
    console.log(loginObj);
   
    
    if (this.user == "" || this.user == undefined || this.user == null||this.user.length<4) {
      this.showEmailerror = true;
    }
    if (this.pass == "" || this.pass == undefined || this.pass == null) {
      this.showPassworderror = true;
    }
    if (this.formassigned == 0 || this.formassigned == undefined || this.formassigned == null) {
      this.showFormAssignederror = true;
    }
    if(!this.showEmailerror&&  !this.showPassworderror && !this.showFormAssignederror){
      this.checkUserExistingorNot(this.user);
    
}

  }
  
}


