import { Component, OnInit } from "@angular/core";
import { UserService } from "../user/user.service";
import { Router } from "@angular/router";
import { map } from 'rxjs/operators';

@Component({
  selector: "app-user-form",
  templateUrl: "./user-form.component.html",
  styleUrls: ["./user-form.component.css"],
})
export class UserFormComponent implements OnInit {
  constructor(private userService: UserService, private router: Router) {}
  candidateName: string;
  email: string;
  contact: string;
  city: string;
  locality: string;
  role: string;
  qualification: string;
  skill: string;
  dob: string;
  age: string;
  Maxerror:any;
  password:any;
  languageKnown: string;
  singleOrMarried: string;
  fresherOrExperienced: string;
  user: any;
  SuccessCount: number = 0;
  ErrorCount: number = 0;
  formAssigned: number=0;
  Formindex: number;
  formob: any;
  captcha: string;
  submitObj: any;
  showtable: boolean=false;
  formsSubmitted: any
  userData: any
  userId: string;
  emptyCheck:boolean=false;
  EmptyRecords:Array<string>;
  Invalidrecords: Array<{ key: any, correctValue: any, incorrectValue: any }>;
  ngOnInit(): void {
    if (!this.userService.getUser()) {
      this.router.navigate(["login"]);
    } else {
      let UsersInf=[];
      this.Invalidrecords = [{ key: undefined, correctValue: undefined, incorrectValue: undefined }]
      this.user = this.userService.getUser();
      this.userService.getAllUSerList().subscribe((data) => {
        let keys = [];
        keys = Object.keys(data);
        keys.forEach((key, index) => {
          UsersInf.push({'userName':data[key].userName,'succes': data[key].successCount,'error': data[key].errorCount,
          'Maxerror': data[key].Maxerror,
          'formassigned': data[key].formsAssigned,
          'formsubmitted': data[key].formsSubmitted,
          'password': data[key].passWord,
          'status': data[key].status,
          'key':key}); 
        });
        UsersInf.forEach(element => {
          if(element.userName==this.user)
          {
            this.userId=element.key;
            if(element.error>= element.Maxerror){
              this.router.navigate(["login"]);
            }
            if (element!== undefined) {
              this.ErrorCount = element.error;
              this.SuccessCount = element.succes;
              this.Formindex = element.formsubmitted;
             this.formAssigned= element.formassigned; 
             this.password= element.password; 
             this.Maxerror=element.Maxerror;
             if(element.status!="Active"){
              this.router.navigateByUrl("login");
             }  
             if(this.Formindex>= this.formAssigned-1) {
               this.router.navigateByUrl("login");
             } 
             this.formsSubmitted = this.Formindex
             this.formob = [];
             this.userService.getAllFormsList().subscribe((data) => {
               this.formob.push(this.RemoveDoubleSpacingProblem(data[Object.keys(data)[this.Formindex]]));
             //  this.formob.push(data['-MAtn9v8bYciJZiFSlvi']);
               console.log(data);
             });
          }
        }
      });
    
      





    //   this.userService.checkUsers(this.user).subscribe((data) => {
    //     this.userData = data
    //     this.userId = Object.keys(data)[0]
    //     if(data[Object.keys(data)[0]].errorCount>=data[Object.keys(data)[0]].Maxerror){
    //       this.router.navigate(["login"]);
    //     }
    //     if (data[Object.keys(data)[0]] !== undefined) {
    //       this.ErrorCount = data[Object.keys(data)[0]].errorCount;
    //       this.SuccessCount = data[Object.keys(data)[0]].successCount;
    //       this.Formindex = data[Object.keys(data)[0]].formsSubmitted;
    //      this.formAssigned= data[Object.keys(data)[0]].formsAssigned; 
    //      this.password= data[Object.keys(data)[0]].passWord; 
    //      this.Maxerror=data[Object.keys(data)[0]].Maxerror;
    //      if(data[Object.keys(data)[0]].status!="Active"){
    //       this.router.navigateByUrl("login");
    //      }  
    //      if(this.Formindex>= this.formAssigned-1) {
    //        this.router.navigateByUrl("login");
    //      } 
    //       this.formsSubmitted = this.Formindex
    //       this.formob = [];
    //       this.userService.getAllFormsList().subscribe((data) => {
    //         this.formob.push(this.RemoveDoubleSpacingProblem(data[Object.keys(data)[this.Formindex]]));
    //       //  this.formob.push(data['-MAtn9v8bYciJZiFSlvi']);
    //         console.log(data);
    //       });
    //     }
    //   });
    // }
    //   )};
  });
}
  }
  RemoveDoubleSpacingProblem(obj:any){
    let AllFields =[];

    
      let k = Object.keys(obj);
      console.log(k);
      k.forEach((element) => {
        obj[element]=  obj[element].replace(/ {2,}/g, ' ')
        .trim()
      });
    
    // if(obj.qualification=="Graduate - B.Com  Commerce"){
    //   obj.qualification="Graduate - B.Com Commerce"
    // }
    // else if(obj.qualification=="Graduate - BA  Arts And Humanities"){
    //   obj.qualification="Graduate - BA Arts And Humanities"
    // }
    return obj;
  }


  submit(submitObj) {
    this.submitObj = submitObj;
    this.checkValidRecords(this.submitObj);
    this.showtable=undefined;
   
    if (this.Invalidrecords.length !== 0 && this.Invalidrecords !== [{ key: undefined, correctValue: undefined, incorrectValue: undefined }]||this.EmptyRecords.length!=0) {
      this.ErrorCount++;
      this.formsSubmitted++;

    } else {
      this.SuccessCount++;
      this.formsSubmitted++;
    
    }
    this.userService.Update1User(this.user,this.password,this.formAssigned, "Active",this.formsSubmitted, this.ErrorCount,this.SuccessCount,this.Maxerror, this.userId )
    .subscribe((data) => {
      if (this.Invalidrecords.length !== 0 && this.Invalidrecords !== [{ key: undefined, correctValue: undefined, incorrectValue: undefined }]||this.EmptyRecords.length!=0) {
        this.showtable=true;
  
      } else {
        this.showtable=false;
      
      }
      console.log('update', data)
    })
  }
  resetForm(){

  }
  
  checkValidRecords(submitObj: any) {
    this.Invalidrecords = [];
    this.EmptyRecords=[];
    this.emptyCheck=false;
    for (const iterator of this.formob) {
      let k = Object.keys(iterator);
      console.log(k);
      k.forEach((element) => {
        if (submitObj[element] === undefined || (submitObj[element].replace(/^\s+|\s+$/g, '') != this.formob[0][element].replace(/^\s+|\s+$/g, ''))) {
          if(submitObj[element] === undefined){
            this.emptyCheck=true;
            this.EmptyRecords.push(element);
          }
          else{
          this.Invalidrecords.push({ key: element, correctValue: this.formob[0][element], incorrectValue: submitObj[element] });
        }}
      });
    }
  }
  logout() {
    this.userService.removeUser();
    this.router.navigate(["login"]);
  }

  // pay() {
  //   this.userService.getCheckSum().pipe(
  //     map((response: Response) => {
  //       console.log(response.json());
  //       response.json();
  //     })
      
  //   ).subscribe((checkSumAndOId) => {
  //     console.log('pay', checkSumAndOId)
  //     this.userService.getSubwalletGuid(checkSumAndOId[0]).subscribe((subwallet) => {
  //       console.log('pay', subwallet)
  //       // this.userService.pay(checkSum, subWalletGuid, orderId).subscribe((data) => {
  //       //   console.log('pay', data)
  //       // })
  //     })
  //   })
  // }

  close() {
    this.submitObj = undefined
    location.reload();
    this.Invalidrecords = [{ key: undefined, correctValue: undefined, incorrectValue: undefined }];
  }

  ok() {
    this.submitObj = undefined
    location.reload();
    this.Invalidrecords = [{ key: undefined, correctValue: undefined, incorrectValue: undefined }];
  }
 }
