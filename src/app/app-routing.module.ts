import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { UserFormComponent } from './user-form/user-form.component';
import { AuthGuard } from './auth.guard';
import { SignupComponent } from './signup/signup.component';
import { UsersDetailsComponent } from './users-details/users-details.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent},
  {path: 'user', component: UserFormComponent, canActivate: [AuthGuard]},
  {path: 'Admin', component: AdminPanelComponent},
  {path:'UsersDetails', component:UsersDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,  { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
