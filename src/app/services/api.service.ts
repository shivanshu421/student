import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  addUser(
    username,
    password,
    FormsAssigned,
    Status,
    FormsSubmitted,
    Errorcount,
    Successcount,
    Maxerror
  ): Observable<any> {
    let obj = {
      id: Math.random().toString(),
      userName: username,
      passWord: password,
      formsAssigned: FormsAssigned,
      status: Status,
      formsSubmitted: FormsSubmitted,
      errorCount: Errorcount,
      successCount: Successcount,
      Maxerror: Maxerror
    }
    return this.http.post("https://onlineform-29cde.firebaseio.com/user2.json",
    { ...obj, id: null})
  }
 

  addRecords(
  CandidateName,
  Email,
  Contact,
  City,
  Locality,
  Role,
  Qualification,
  Skill,
  dob,
  Age,
  LanguageKnown,
  SingleOrMarried,
  FresherOrExperienced,
  Captcha
  ): Observable<any> {
    let obj = {
      candidateName: CandidateName,
      email: Email,
      contact: Contact,
      city: City,
      locality: Locality,
      role: Role,
      qualification: Qualification,
      skill: Skill,
      dob: dob,
      age: Age,
      languageKnown: LanguageKnown,
      singleOrMarried: SingleOrMarried,
      fresherOrExperienced: FresherOrExperienced,
      captcha: Captcha
  }
  return this.http.post("https://onlineform-29cde.firebaseio.com/records.json",
  { ...obj })
}

}
