import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user.service';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {
  userdetails: Array<any> = [];
  userInformation: boolean = true;
  userRegister: boolean = false;
  userEdit: boolean = false;
  PageRedirect: string = "Home";
  user: string;
  pass: string;
  formassigned: number;
  showEmailerror: boolean;
  showFormAssignederror: boolean;
  showPassworderror: boolean;
  booksRef: AngularFireList<any>;
  bookRef: AngularFireObject<any>;
  errorCount: any
  formsAssigned: any
  formsSubmitted: any
  passWord: any
  Maxerror:any
  status: any
  Status=["Active","Expire"]
  successCount: any
  userName: any="";
  userId="";
    constructor(private userService: UserService, private router: Router, private apiService: ApiService, private db: AngularFireDatabase) { }

ngOnInit(): void {
  this.getusers();

}
getusers(){
  this.userdetails = [];
  this.userService.getAllUSerList().subscribe((data) => {
    let keys = [];
    keys = Object.keys(data);
    keys.forEach((key, index) => {


      this.userdetails.push({
        error: data[key].errorCount,
        Maxerror: data[key].Maxerror,
        formassigned: data[key].formsAssigned,
        formsubmitted: data[key].formsSubmitted,
        password: data[key].passWord,
        status: data[key].status,
        succes: data[key].successCount,
        username: data[key].userName,
        key1: key
      })


    });
    console.log(this.userdetails)
  });
}
toggleSidebar() {
  document.getElementById("sidebar").classList.toggle('active');
  document.getElementById("MoveSide").classList.toggle('active');
}
UserRegister() {
  console.log("hello");
  this.PageRedirect = "User Registration";
  this.userInformation = false;
  this.userRegister = true;
  this.userEdit = false;
}

UserInfo() {
  this.getusers();
  this.PageRedirect = "Home";
  this.userInformation = true;
  this.userRegister = false;
  this.userEdit = false;
}

Editdelete() {
  console.log("edited");
  this.getusers();
  this.PageRedirect = "Edit/Delete";
  this.userInformation = false;
  this.userRegister = false;
  this.userEdit = true;
}

Logout() {
  this.router.navigate(["login"]);
}
numberOnly(event): boolean {
  const charCode = (event.which) ? event.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;

}
checkUserExistingorNot(usercheck: string){
 let User=[];
  this.userService.getAllUSerList().subscribe((data) => {
    let keys = [];
    keys = Object.keys(data);
    keys.forEach((key, index) => {
      User.push(data[key].userName); 
    });
    if(User.includes(usercheck)){
      alert("user allready exist");
    }
    else{
    
      this.apiService.addUser(
        this.user,
        this.pass,
        this.formassigned,
        "Active",
        0,
        0,
        0,
        20
      ).subscribe((data) => {
        console.log(data)
        alert("user added Successfully");
      })
      
    }
  });

  

  


}
DeleteRecord(key){
  this.bookRef = this.db.object('user2/' + key);
  this.bookRef.remove()

    .catch(error => {
      //this.errorMgmt(error);
    })
  this.getusers();
}

Selectstatus(e){
  this.status=e.target.value;
}
updateUser(user){
  this.userService.getUser1(user).subscribe((data) => {
    this.userName=data.userName;
    this.formsAssigned=data.formsAssigned;
    this.formsSubmitted=data.formsSubmitted;
    this.successCount=data.successCount;
    this.errorCount=data.errorCount;
    this.Maxerror=data.Maxerror;
    this.passWord=data.passWord;
    this.status=data.status;
    this.userId=user;
  });
}
update(){
  this.userService.Update1User(this.userName,this.passWord,this.formsAssigned,this.status,this.formsSubmitted, this.errorCount,this.successCount,this.Maxerror, this.userId )
    .subscribe((data) => {
      this.getusers();
    });
}
signup(loginObj) {
  this.showEmailerror = false;
  this.showPassworderror = false;
  this.showFormAssignederror = false;
  console.log(loginObj);


  if (this.user == "" || this.user == undefined || this.user == null || this.user.length < 4) {
    this.showEmailerror = true;
  }
  if (this.pass == "" || this.pass == undefined || this.pass == null) {
    this.showPassworderror = true;
  }
  if (this.formassigned == 0 || this.formassigned == undefined || this.formassigned == null) {
    this.showFormAssignederror = true;
  }
  if (!this.showEmailerror && !this.showPassworderror && !this.showFormAssignederror) {
    this.checkUserExistingorNot(this.user);

  }

}

}
