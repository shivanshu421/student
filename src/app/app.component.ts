import { Component } from '@angular/core';
import { ApiService } from './services/api.service';
import { HttpClient } from "@angular/common/http";
import { UserService } from './user/user.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private apiService: ApiService,private httpClient: HttpClient,private userService: UserService) {}

  ngOnInit() {
//this.addUser();
//this.transferRecords();
  }
addAllRecords(dt:any){
console.log(dt["Sheet1"]);
// dt["Sheet1"].forEach(element => {
 
  // this.apiService.addRecords(
  //   element["CANDIDATE NAME"],
  //   element["EMAIL ID"],
  //   element["CONTACT"],
  //   element["CITY "],
  //   element["LOCALITY "],
  //   element["ROLE"],
  //   element["QUALIFICATION"],
  //   element["SKILL"],
  //   element["dob"],
  //   element["age"],
  //   element["languageKnown"],
  //   element["singleOrMarried"],
  //   element["fresherOrExperienced"],
  //   element["captcha"]
  // ).subscribe((data) => {
  //   console.log(data)
  // })
  
// })
}

  addUser() {
    this.apiService.addUser(
      "demo7@ask.com",
      "demo",
      10,
      "Expire",
      0,
      0,
      0,
      20
    ).subscribe((data) => {
      console.log(data)
    })
  }

  transferRecords(){
    this.userService.getAllUSerList().subscribe((data) => {
      let keys = [];
      keys = Object.keys(data);
      keys.forEach((key, index) => {
        this.apiService.addUser(
          data[key].userName,
          data[key].passWord,
          data[key].formsAssigned,
          data[key].status,
          data[key].formsSubmitted,
          data[key].errorCount,
          data[key].successCount,
          20
        ).subscribe((data) => {
          console.log(data)
        })
      });
        
  
  
     
      // console.log(this.userdetails)
    });
  }
  addRecords() {
    this.apiService.addRecords(
      "Rajesh5",
      "Raajesh588222@rediffmail.com",
      "9420829641",
      "Pune",
      "Camp",
      "Data Entry/ Back Office",
      "Graduate - B.Com",
      "NO",
      "09/21/1993",
      "26",
      "Hindi/English",
      "Single",
      "5 years",
      "asfzdf2"
    ).subscribe((data) => {
      console.log(data)
    })
  }
}
