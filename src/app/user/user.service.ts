import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import { Observable, of, throwError } from "rxjs";
import { take, map, delay, tap, switchMap, catchError } from "rxjs/operators";
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: "root",
})
export class UserService {
  // isUser: boolean
  user: string;
  randomAlphNumeric: any
  Url: String = 'https://localhost:44386/Paytm/Paytmcheck';
  headers: any;

  constructor(private http: HttpClient) {
    this.headers = new Headers();
    const user = sessionStorage.getItem("user");
    if (user) {
      this.user = user;
    }
  }


  getValidateUser(): boolean {
    return !!sessionStorage.getItem("user");
  }

  setUser(user) {
    this.user = user;
    sessionStorage.setItem("user", user);
  }

  getUser() {
    return this.user;
  }

  removeUser() {
    this.user = "";
    sessionStorage.removeItem("user");
  }

  checkUsers(user): Observable<any> {
    return this.http.get(
      `https://onlineform-29cde.firebaseio.com/user2.json?orderBy="userName"&equalTo="${user}"`
    );
  }

  getUser1(key):Observable<any>{
    return this.http.get(
      'https://onlineform-29cde.firebaseio.com/user2/'+key+'.json'
    );
  }


  UpdateUser(
    userData,
    userId,
    formsubmitted,
    succeesCount,
    errorCount
  ): Observable<any> {
    let updatedUserData: any;
    return of(userData).pipe(
      take(1),
      switchMap((user) => {
        if (user) {
          return of(user);
        }
      }),
      switchMap((user) => {
        updatedUserData = user;
        const oldData = updatedUserData[userId];
        updatedUserData[userId] = {
          id: oldData.id,
          errorCount: errorCount,
          formsAssigned: 10,
          formsSubmitted: formsubmitted,
          passWord: "demo",
          status: "Active",
          successCount: succeesCount,
          userName: "demo@ask.com",
        };
        return this.http.put(
          `https://onlineform-29cde.firebaseio.com/user2.json`,
          {
            ...updatedUserData,
            id: null,
          }
        );
      })
    );
  }
  
  getAllFormsList() {
    return this.http.get(
      `https://onlineform-29cde.firebaseio.com/records.json?`
    );
  }
  getAllUSerList() {
    return this.http.get(
      `https://onlineform-29cde.firebaseio.com/user2.json?`
    );
  }

  Update1User(
    username,
    password,
    FormsAssigned,
    Status,
    FormsSubmitted,
    Errorcount,
    Successcount,
    Maxerror,
    key
  ): Observable<any> {
    let obj = {
      id: Math.random().toString(),
      userName: username,
      passWord: password,
      formsAssigned: FormsAssigned,
      status: Status,
      formsSubmitted: FormsSubmitted,
      errorCount: Errorcount,
      successCount: Successcount,
      Maxerror:Maxerror
    }
    return this.http.put("https://onlineform-29cde.firebaseio.com/user2/"+key+".json",
    { ...obj, id: null}).pipe(
      catchError(this.handleError)
    );
  }
  makeRandom(lengthOfCode: number, possible: string) {
    let text = "";
    for (let i = 0; i < lengthOfCode; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
      return text;
  }

  getCheckSum(): Observable<any> {
   
  
    const url = `${this.Url}`;
    
    return this.http
      .get(url) .pipe(
        catchError(this.handleError)
      );
     
 
  }


  // getSubwalletGuid(checkSum): Observable<any> {
  //   let headers = new HttpHeaders({
  //     "Content-Type": "application/json",
  //     "x-mid": environment.mId,
  //     "x-checksum": checkSum,
  //   });
  //   let options = { headers: headers };
  //   let data = []
  //   return this.http.post(`https://staging-dashboard.paytm.com/bpay/api/v1/account/list`, data, options)
  // }

  // pay(checkSum, subWalletGuid, orderId): Observable<any> {
  //   let headers = new HttpHeaders({
  //     "Content-Type": "application/json",
  //     "x-mid": environment.mId,
  //     "x-checksum": checkSum,
  //   });
  //   let options = { headers: headers };
  //   let data = {
  //     subwalletGuid: subWalletGuid,
  //     orderId: orderId,
  //     beneficiaryPhoneNo: "9319554720",
  //     amount: "1.00",
  //   };
  //   return this.http.post(
  //     `https://staging-dashboard.paytm.com/bpay/api/v1/disburse/order/wallet/{solution}`, data, options
  //   );
  // }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
// ab yahan pr dono pr debugger lagana krke dekh error aayega isme ki kya aa