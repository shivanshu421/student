import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users-details',
  templateUrl: './users-details.component.html',
  styleUrls: ['./users-details.component.css']
})
export class UsersDetailsComponent implements OnInit {
//userDetail {username:any,password:any,Formsubmitted:any,FormAssigned:any,error:any,status:any,succes:any};
userdetails : Array<any>=[];
  constructor(private userService: UserService,private router: Router) { }

  ngOnInit(): void {
    
    
    this.userService.getAllUSerList().subscribe((data) => {
     let keys=[] ;
     keys=Object.keys(data);
     keys.forEach( (key, index) => {
      
  
      this.userdetails.push({error:data[key].errorCount,
        formassigned:data[key].formsAssigned,
        formsubmitted:data[key].formsSubmitted ,
        password: data[key].passWord,
        status:data[key].status,
        succes:data[key].successCount,
        username:data[key].userName})
      
    
    });
    console.log(this.userdetails)
  });
  }
  backToSignup(){
    this.router.navigateByUrl("signup");
  }
  }
